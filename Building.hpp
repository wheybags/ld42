#ifndef __BUILDING_HPP__
#define __BUILDING_HPP__

#include <memory>
#include <SFML/Graphics.hpp>

class Building : public sf::Transformable
{
public:
  Building();

  void draw(sf::RenderWindow &w, sf::Transform parentTransform = sf::Transform());
  sf::Vector2u getSize() const;

private:
  std::shared_ptr<sf::Texture>  _texture;
  sf::Sprite                    _sprite;
  float                         _height;
};

#endif
