#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include "Planet.hpp"
#include "shaders.hpp"
#include "util.hpp"
#include "System.hpp" // For polar coordinate conversion, needs cleanup

Planet::Planet(System& system, float radius, const std::vector<std::string>& imageNames, sf::Color glowColor, std::string const &name, bool addBuildings)
    : _radius(radius)
    , _color(glowColor)
    , _name(name)
    , _system(system)
    , _hookLenght(-1)
{
  for (const auto& name: imageNames)
  {
    std::unique_ptr<sf::Texture> _texture = std::unique_ptr<sf::Texture>(new sf::Texture());

    if (!_texture->loadFromFile(Util::resourcePath + name))
      std::cerr << "can't load" << std::endl;

    _texture->generateMipmap();
    _texture->setSmooth(true);

    _textures.push_back(std::move(_texture));
  }

  sf::Texture* firstFrame = _textures.front().get();

  _textureSprite.setTexture(*firstFrame, false);
  _glowSprite.setTexture(*firstFrame, false);

  _textureSprite.setOrigin(firstFrame->getSize().x / 2, firstFrame->getSize().y / 2);
  _glowSprite.setOrigin(firstFrame->getSize().x / 2, firstFrame->getSize().y / 2);
  Util::resizeSprite(_glowSprite, firstFrame->getSize().x * 2, firstFrame->getSize().y * 2);

  setScale(sf::Vector2f(radius, radius));
  _speed = static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - .5;
  _angularSpeed = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX) - .5);// / 10;

  if (addBuildings)
    addBuilding();
}

void Planet::addBuilding()
{
  _buildings.push_back(Building());
  int i(0);
  for (Building &b : _buildings)
  {
    sf::Vector2f pol(-240 /* MAGIC NUMBER !!! */, 0);
    pol.y += (2 * M_PI) / _buildings.size() * i;
    b.setPosition(System::polToCart(pol));
    b.setRotation(pol.y * 57.2958 - 90);
    i++;
  }
}

void Planet::shootHook()
{
  if (_hookLenght < 0)
    _hookLenght = 1; //_radius * _texture->getSize().x;
}

void Planet::drawHook(sf::RenderWindow &w, sf::Transform parentTransform)
{
  sf::RectangleShape rect(sf::Vector2f(10, _hookLenght));
  rect.rotate(90);
  w.draw(rect, parentTransform);
}


void Planet::draw(sf::RenderWindow &w, sf::Transform cameraTransform, sf::Vector2f origin)
{
  sf::Transform scaleTransform;
  scaleTransform.scale(this->_radius, this->_radius);

  sf::Transform renderTransform;
  renderTransform.translate(origin);
  renderTransform.translate(this->getPosition());
  renderTransform = cameraTransform * renderTransform * scaleTransform;


  for (Building &b : _buildings)
    b.draw(w, renderTransform);

  auto zero = cameraTransform.transformPoint(0, 0);
  auto one = cameraTransform.transformPoint(0, 1);

  auto between = zero - one;
  float scalingFactor = Util::length(between);

  float glowRangeMin = 0.3f;
  float glowRangeMax = 0.6f;
  float glowSpeed = 3.0f;

  float glowWidth = (sinf(_totalDelta * glowSpeed) + 1.0f) / 2.0f; // get sin wave in 0-1 range
  glowWidth = (glowWidth * (glowRangeMax-glowRangeMin)) + glowRangeMin; // scale into desired range

  size_t frameIndex = size_t(std::floor(std::fmod(this->_totalDelta * 10.0f, float(this->_textures.size()))));
  _textureSprite.setTexture(*_textures[frameIndex].get());




  auto& shader = Shaders::get()->planetGlow;
  auto centre = renderTransform.transformPoint(this->getOrigin());

//      (cameraTransform * getTransform()).transformPoint(this->getOrigin());
  centre.y = w.getSize().y - centre.y; // screw you opengl
  shader.setUniform("centre", centre);
  shader.setUniform("radius", float(_textureSprite.getTexture()->getSize().x / 2 * _radius * scalingFactor));
  shader.setUniform("glowWidth", glowWidth);
  shader.setUniform("glowColor", sf::Glsl::Vec4(_color));


  sf::RenderStates renderState;
  renderState.shader = &shader;
  renderState.transform = renderTransform;//parentTransform * this->getTransform();

  w.draw(_glowSprite, renderState);
  w.draw(_textureSprite, renderTransform);

  drawHook(w, renderTransform);
}

void Planet::update(float dt)
{
  dt *= _updateSpeed;

  // float angle = Util::radToDeg(atan2f(this->getPosition().y, this->getPosition().x));
  // this->setRotation(angle);
  rotate(dt * _angularSpeed * 50);
  _totalDelta += dt;

  if (_hookLenght >= 0)
    {
      _hookLenght += 100;
      if (_hookLenght > 50000)
        _hookLenght = -1;
    }
}

bool Planet::handleClick(sf::Vector2f const &p)
{
  sf::Vector2f localPoint = getInverseTransform().transformPoint(p);
  if (_textureSprite.getGlobalBounds().contains(localPoint))
  {
    //    shootHook();
    //    addBuilding();
    return true;
  }

  return false;
}
