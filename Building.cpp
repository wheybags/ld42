#include "Building.hpp"
#include "util.hpp"

Building::Building()
  :_height(0.0f)
{
  _texture = std::make_shared<sf::Texture>();
  _texture->loadFromFile(Util::resourcePath + "res/building" + std::to_string(rand() % 3 + 1) + ".png");
  _sprite.setTexture(*_texture);
  setOrigin(_texture->getSize().x / 2, _texture->getSize().y);
}

void Building::draw(sf::RenderWindow &w, sf::Transform parentTransform)
{
  w.draw(_sprite, parentTransform * this->getTransform());
}

sf::Vector2u Building::getSize() const
{
  return _texture->getSize();
}
