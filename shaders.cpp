#include "shaders.hpp"
#include <cassert>
#include "util.hpp"


void loadShader(sf::Shader& shader, const std::string& name, sf::Shader::Type type)
{
  bool success = shader.loadFromFile(Util::shadersPath + name, type);
  assert(success);

//    if (type == sf::Shader::Type::Fragment)
//        shader.setUniform("texture", sf::Shader::CurrentTexture);

}

Shaders::Shaders()
{
  loadShader(this->planetGlow, "planetGlow.frag", sf::Shader::Type::Fragment);
}

static Shaders* instance = nullptr;
void Shaders::init()
{
  assert(!instance);
  instance = new Shaders();
}

Shaders* Shaders::get()
{
  assert(instance);
  return instance;
}

void Shaders::destroy()
{
  delete instance;
  instance = nullptr;
}
