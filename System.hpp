#ifndef __SYSTEM_HPP__
#define __SYSTEM_HPP__

#include "Planet.hpp"
#include <memory>

class System : public sf::Transformable
{
public:
  System();

  void draw(sf::RenderWindow &w, sf::Transform cameraTransform);
  void drawBridges(sf::RenderWindow &w, sf::Transform fullTransform);
  void update(float dt);
  bool handleClick(sf::Vector2f const &p);

  float Dot( const sf::Vector2f& a, const sf::Vector2f& b );
  void drawLine(std::pair<sf::Vector2f, sf::Vector2f> const &iline, sf::RenderWindow &w, sf::Transform fullTransform);

  static sf::Vector2f cartToPol(sf::Vector2f const & cart);
  static sf::Vector2f polToCart(sf::Vector2f const & pol);

  Planet& getSun() { return *_planets[0].get(); }
  sf::Vector2f getActivePosition();

  void collisionDetection();
  std::vector<std::pair<sf::Vector2f, sf::Vector2f>> getBridges() const;
  float lineToPointDistance(std::pair<sf::Vector2f, sf::Vector2f> const &line, sf::Vector2f const &point);
private:
  std::vector<std::shared_ptr<Planet>> _planets;
  std::shared_ptr<Planet> _activePlanet;
};

#endif
