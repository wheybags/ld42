#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <cassert>
#include <chrono>
#include <thread>

#include "shaders.hpp"
#include "System.hpp"
#include "util.hpp"

int main()
{
  assert(sf::Shader::isAvailable());
  srand (time(NULL));
  sf::RenderWindow window(sf::VideoMode(1200, 800), "Running Out Of Space!");
  window.setVerticalSyncEnabled(true);
  Shaders::init();

  float zoom(0.3);

  System solar;
  solar.setPosition(300, 0);


  std::chrono::time_point<std::chrono::steady_clock> lastFrameEnd = std::chrono::steady_clock::now();

  sf::Transform viewTransform;
  sf::Vector2f movement;





  sf::Texture _texture;
  if (!_texture.loadFromFile(Util::resourcePath +  "bg-1.png"))
    std::cerr << "can't load" << std::endl;

  _texture.generateMipmap();
  _texture.setSmooth(true);
  _texture.setRepeated(true);

  sf::Sprite _textureSprite;
  _textureSprite.setTexture(_texture, true);
//  _textureSprite.scale(9999, 9999);
  _textureSprite.scale(0.5, 0.5);
  _textureSprite.setTextureRect(sf::IntRect(0, 0, _texture.getSize().x*10000, _texture.getSize().y*10000));

  _textureSprite.setPosition(-_textureSprite.getGlobalBounds().width/2, -_textureSprite.getGlobalBounds().height/2);





  while (window.isOpen())
  {
    viewTransform = sf::Transform();
    viewTransform.translate(solar.getActivePosition());


//    auto zoomedTransform = viewTransform;
    sf::Transform zoomedTransform;
    zoomedTransform.translate(window.getSize().x/2, window.getSize().y/2);
    zoomedTransform.scale(sf::Vector2f(zoom, zoom));
    zoomedTransform = zoomedTransform * viewTransform;

    sf::Event event;
    while (window.pollEvent(event))
    {
      if (event.type == sf::Event::Closed)
        window.close();
      if (event.type == sf::Event::MouseButtonPressed)
        solar.handleClick(zoomedTransform.getInverse().transformPoint(sf::Vector2f(event.mouseButton.x, event.mouseButton.y)));
      if (event.type == sf::Event::MouseWheelScrolled)
        zoom += event.mouseWheelScroll.delta / 100.0f;
    }


    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - lastFrameEnd);
    float seconds = float(duration.count()) / 1000.0f;


    movement = sf::Vector2f();

    float zoomDelta = zoom / 1000.0f;

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
      movement += sf::Vector2f(1, 0);
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
      movement += sf::Vector2f(-1, 0);
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
      movement += sf::Vector2f(0, 1);
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
      movement += sf::Vector2f(0, -1);
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
      zoom += zoomDelta;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::X))
      zoom -= zoomDelta;

    zoom = std::max(std::min(5.0f, zoom), 0.01f);

//      zoom *= 5;

    std::cout << zoom << std::endl;

    Util::normalize(movement);

    float movementScale = seconds * 400.0f;
    movement.x *= movementScale;
    movement.y *= movementScale;
    viewTransform.translate(movement.x, movement.y);


    solar.update(seconds);

    lastFrameEnd = std::chrono::steady_clock::now();

    window.clear();

    sf::Transform bgTransform;
    bgTransform.translate(window.getSize().x/2, window.getSize().y/2);

    float bScale = 1.0f + zoom / 10.0f;
    bgTransform.scale(sf::Vector2f(bScale, bScale));
    bgTransform.translate(solar.getActivePosition() / 100.0f);

    window.draw(_textureSprite, bgTransform);
    solar.draw(window, zoomedTransform);
    window.display();
  }

  return 0;
}
