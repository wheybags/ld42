#pragma once

#include <cstdint>
#include <SFML/Graphics.hpp>


namespace Util
{
  void resizeSprite(sf::Sprite& sprite, float w, float h);
  float length(sf::Vector2f v);
  void normalize(sf::Vector2f& v);

  float radToDeg(float rad);

  extern std::string resourcePath;
  extern std::string shadersPath;
}
