#include <cassert>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include "System.hpp"
#include "util.hpp"
#include <SFML/Graphics.hpp>

System::System()
{
  auto p = std::unique_ptr<Planet>(new Planet(*this, 5, {
                                                "sun-1.png",
                                                "sun-2.png",
                                                "sun-3.png",
                                                "sun-4.png",
                                                "sun-5.png",
                                                "sun-6.png",
                                              }, sf::Color(181, 27, 15), "THE SUN", false));
  p->setPosition(0, 0);
  _planets.push_back(std::move(p));

  for (int i(1) ; i < 5 ; ++i)
  {
    float radius = 1; // fmax(.1, static_cast <float> (rand()) / static_cast <float> (RAND_MAX));
    auto p = std::shared_ptr<Planet>(new Planet(*this, radius, {"globe.png"}, sf::Color(0*256, 0.2*256, 0.5*256), "Planet " + std::to_string(i)));
    p->setPosition(i * 1000 + 5000, 0);
    _planets.push_back(p);
  }

  this->_activePlanet = this->_planets.back();
}

void System::draw(sf::RenderWindow &w, sf::Transform cameraTransform)
{
  for (auto& p : _planets)
    p->draw(w, cameraTransform, this->getPosition());

  drawBridges(w, cameraTransform);//, this->getPosition());
}

void System::drawBridges(sf::RenderWindow &w, sf::Transform cameraTransform)
{
  cameraTransform.translate(this->getPosition());
  auto bridges = getBridges();
  for (auto b : bridges)
    drawLine(b, w, cameraTransform);
}

void System::update(float dt)
{
  for (auto& p : _planets)
  {
    p->_color = sf::Color(0, 0, 255);
    sf::Vector2f pol = cartToPol(p->getPosition());
    pol.y += dt * p->getSpeed();
    p->setPosition(polToCart(pol));
    p->update(dt);
  }
  _activePlanet->_color = sf::Color(0, 255, 0);
  _planets[0]->_color = sf::Color(255, 0, 0); // THE SUN !
  collisionDetection();
}

bool System::handleClick(sf::Vector2f const &p)
{
  sf::Vector2f p2 = p - this->getPosition();//getInverseTransform().transformPoint(p);
  for (size_t i = 0; i < _planets.size(); i++)
  {
    if (_planets[i]->handleClick(p2))
    {
      _activePlanet->next = _planets[i];
      _activePlanet = _planets[i];
    }
  }
  return false;
}

sf::Vector2f System::getActivePosition()
{
  auto point = this->_activePlanet->getPosition() + this->getPosition();

  point.y = -point.y;
  point.x = -point.x;

  return point;
}

sf::Vector2f System::cartToPol(sf::Vector2f const & cart)
{
  if (cart == sf::Vector2f())
    return sf::Vector2f();

  sf::Vector2f pol;
  pol.x = sqrt(cart.x * cart.x + cart.y * cart.y);
  pol.y = atan(cart.y / cart.x);
  if (cart.x < 0)
    pol.y += M_PI;
  return pol;
}

sf::Vector2f System::polToCart(sf::Vector2f const & pol)
{
  sf::Vector2f cart;
  cart.x = pol.x * cos(pol.y);
  cart.y = pol.x * sin(pol.y);
  return cart;
}

std::vector<std::pair<sf::Vector2f, sf::Vector2f>> System::getBridges() const
{
  std::vector<std::pair<sf::Vector2f, sf::Vector2f>> bridges;
  for (std::shared_ptr<Planet> p : _planets)
    {
      if (p->next)
        bridges.push_back(std::make_pair(p->getPosition(), p->next->getPosition()));
    }
  return bridges;
}

void System::collisionDetection()
{
  auto bridges = getBridges();
  for (auto &b : bridges)
    {
      //      std::cout << "[" << b.first.x << " / " << b.first.y << "]-[" << b.second.x << " / " << b.second.y << "]" << std::endl;
      for (std::shared_ptr<Planet> p : _planets)
        {
          float d = lineToPointDistance(b, p->getPosition());
          std::cout << p->_name << " --> " << d << std::endl;
          if (d > 0 && d < 10)
            std::cout << "Collision" << std::endl;

          // std::cout << "Distance to " << p->_name << "  : " << d << std::endl;
        }
    }
}

  float System::Dot( const sf::Vector2f& a, const sf::Vector2f& b )
  {
    return a.x * b.x + a.y * b.y;
  }

float System::lineToPointDistance(std::pair<sf::Vector2f, sf::Vector2f> const &iline, sf::Vector2f const &p)
{
  sf::Vector2f a = iline.first;
  sf::Vector2f b = iline.second;

  sf::Vector2f n = b - a;
  sf::Vector2f pa = a - p;
  sf::Vector2f c = n * (Dot( pa, n ) / Dot( n, n ));
  sf::Vector2f d = pa - c;
  return sqrt( Dot( d, d ) );
}

void System::drawLine(std::pair<sf::Vector2f, sf::Vector2f> const &line, sf::RenderWindow &w, sf::Transform fullTransform)
{
  float lenght = sqrt(pow(line.first.x - line.second.x, 2) + pow(line.first.y - line.second.y, 2));
  float angle = Util::radToDeg(atan2f(line.first.y - line.second.y, line.first.x - line.second.x));

  sf::RectangleShape rect(sf::Vector2f(lenght, 10));
  rect.rotate(angle);
  rect.setPosition(line.second);
  w.draw(rect, fullTransform);
}
