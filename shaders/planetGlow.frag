//uniform sampler2D texture;

uniform vec2 centre;
uniform float radius;
uniform float glowWidth;
uniform vec4 glowColor;

void main()
{
    vec4 pixel = glowColor;

    float distance = length(centre - gl_FragCoord.xy);
    distance /= radius;

    float alpha = 1.0 - smoothstep(1.0, 1.0 + glowWidth, distance);
    gl_FragColor = vec4(pixel.rgb, alpha);
}
