#pragma once

#include <SFML/Graphics.hpp>
#include <memory>

class Shaders
{
  Shaders();

public:
  static void init();
  static Shaders* get();
  static void destroy();

  sf::Shader planetGlow;
};
