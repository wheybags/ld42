#ifndef __PLANET_HPP__
#define __PLANET_HPP__

#include <memory>
#include <SFML/Graphics.hpp>
#include "Building.hpp"

class System;

class Planet : public sf::Transformable
{
public:
  Planet(System& system, float radius, const std::vector<std::string>& imageNames, sf::Color glowColor, std::string const &name, bool buildings = true);

  void draw(sf::RenderWindow &w, sf::Transform cameraTransform, sf::Vector2f origin);
  void update(float dt);
  bool handleClick(sf::Vector2f const &p);

  void drawHook(sf::RenderWindow &w, sf::Transform parentTransform);

  void addBuilding();
  void shootHook();

  float getSpeed() const { return _speed; }
  float getRadiusInPixel() const {return _radius * _textures[0]->getSize().x; }

  std::shared_ptr<Planet> next;


//private:
  std::vector<std::unique_ptr<sf::Texture>>  _textures;
  sf::Sprite                                 _textureSprite;
  sf::Sprite                                 _glowSprite;
  float                                      _radius = 0;
  float                                      _totalDelta = 0;
  float                                      _updateSpeed = 1.0f;
  float                                      _speed;
  float                                      _angularSpeed;
  std::vector<Building>                      _buildings;
  sf::Color                                  _color;
  std::string                                _name;
  System&                                    _system;
  float                                      _hookLenght;
};

#endif /* __PLANET_HPP__ */
