#define _USE_MATH_DEFINES
#include "util.hpp"
#include <cmath>

namespace Util
{
  void resizeSprite(sf::Sprite& sprite, float w, float h)
  {
    sf::FloatRect currentSize = sprite.getGlobalBounds();
    sprite.setScale(w / currentSize.width, h / currentSize.height);
  }

  float length(sf::Vector2f v)
  {
    return sqrtf(v.x * v.x + v.y * v.y);
  }

  void normalize(sf::Vector2f& v)
  {
    float _length = length(v);

    if (_length > 0)
    {
      v.x = v.x / _length;
      v.y = v.y / _length;
    }
  }

  float radToDeg(float rad)
  {
    return rad * 180.0f / float(M_PI);
  }

#ifdef WIN32
  std::string resourcePath = "res\\";
  std::string shadersPath = "shaders\\";
#else
  std::string resourcePath = "res/";
  std::string shadersPath = "shaders/";
#endif
}
